//工具类:
public class Utils {
	
	public static final String LOG_TAG = "Utils";

	public static int getHour(){
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Date d = new Date();
	    String s = sdf.format(d);
	    int hour = Integer.parseInt(s.substring(0, 2));
	    Log.d(LOG_TAG, "hour=" + hour);
	    return hour;
	
	}
	public static int getMin() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Date d = new Date();
	    String s = sdf.format(d);
	    int min = Integer.parseInt(s.substring(3));
	    Log.d(LOG_TAG, "min=" + min);
	    return min;
	}
	
	public static String getDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日");
		Date d = new Date();
		String s = sdf.format(d);
		Log.d(LOG_TAG, "date=" + s);
		return s;
	}
	
	public static String getWeekOfDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Date d = new Date();
		String s = sdf.format(d);
		Log.d(LOG_TAG, "week = " + s);
		return s;
	}
}

//自定义View
public class DateView extends View {

	public DateView(Context context) {
		super(context);
	}
	
	public DateView(Context context, AttributeSet attr) {
		super(context, attr);
	}
	
	public DateView(Context context, AttributeSet attr, int style) {
		super(context, attr, style);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		Paint mPaint = new Paint();
		String sHour = null;
		String sMin = null;
		int hour = Utils.getHour();
		int min = Utils.getMin();
		if (hour < 10) 
			sHour = "0" + hour;
		else
			sHour = "" + hour;
		if (min < 10)
			sMin = "0" + min;
		else
			sMin = "" + min;
			
		mPaint.setColor(Color.BLACK);
		mPaint.setTextSize(80.0f);
		mPaint.setStyle(Style.FILL);
		mPaint.setStrokeWidth(3.0f);
		canvas.drawText(sHour + "", 10, getHeight() / 2 + 8, mPaint);
		canvas.drawText(sMin + "", getWidth() / 2 + 10, getHeight() / 2 + 8, mPaint);
		
		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		paint.setStyle(Style.FILL);
		paint.setTextSize(20);
		String date = Utils.getDate();
		canvas.drawText(date, 8, getHeight() / 2 + getHeight() / 3 + 8, paint);
		
		canvas.drawText(Utils.getWeekOfDate(), getWidth() / 2 + date.length(), 
				getHeight() / 2 + getHeight() / 3 + 8, paint);
		
		String am_pm = null;
		if (Utils.getHour() >= 12)
			am_pm = "PM";
		else
			am_pm = "AM";
		canvas.drawText(am_pm, getWidth() - date.length() * 4 - Utils.getWeekOfDate().length() * 4,
				getHeight() / 2 + getHeight() / 3 + 8, paint);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

}

//主界面
public class MainActivity extends Activity {
	
	public static final String LOG_TAG = "MainActivity";
	
	private DateView dv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		dv = (DateView) findViewById(R.id.date);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_TIME_CHANGED);
		filter.addAction(Intent.ACTION_TIME_TICK);
		filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
		filter.addAction(Intent.ACTION_DATE_CHANGED);
		registerReceiver(date_time_changed_receiver, filter);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		try {
			unregisterReceiver(date_time_changed_receiver);
			Log.d(LOG_TAG, "date_time_changed_receiver has unregistered.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private BroadcastReceiver date_time_changed_receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.d(LOG_TAG, "action=" + action);
			if (action.equals(Intent.ACTION_TIME_CHANGED)
					|| action.equals(Intent.ACTION_TIME_TICK)
					|| action.equals(Intent.ACTION_TIMEZONE_CHANGED)
					|| action.equals(Intent.ACTION_DATE_CHANGED)) {
				dv.invalidate();
				Log.d(LOG_TAG, "TIMEZONE / DATA / TIME CHANGED...");
			}
		}
	};
}

//布局文件
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="horizontal"
    android:id="@+id/main_layout"
    android:layout_width="match_parent"
    android:layout_height="match_parent" >
        <com.example.viewdemo.DateView
        android:id="@+id/date"
        android:background="@drawable/weather_bg"
        android:layout_width="228dip"
        android:layout_height="128dip" />
        
</LinearLayout>